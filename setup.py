from setuptools import setup

setup(name='debd',
      version='0.9.1',
      description='The text based adventure!',
      url='http://example.com',
      author='Bernie Sanders',
      author_email='donate@berniesanders.com',
      license='WTFPL',
      packages=['debd'],
      include_package_data=True,
      zip_safe=False,
      entry_points={
          'console_scripts': [
              'debd = debd.debd:main'
          ]
      })