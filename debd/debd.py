"""Debd, The Based Text Adventure!

By Del Norte Engineering

License: WTFPL
"""

import os.path
import random
import signal


things = []
with open(os.path.dirname(__file__) + '/lines.txt', 'r') as lines:
    for line in lines:
        things.append(line)


skeptical_things = [
    "Are you sure you want to do that?",
    "I don't think that is a very good idea.",
    "Please stop trying to do that.",
    "OH MY GOD WHAT ARE YOU DOING",
    "No."
]


def respond(msg):
    if random.random() < 0.1:
        print(random.choice(skeptical_things))
    else:
        print(msg)


def main():    
    print("""\n\n
    WELCOME TO DEBD, THE BASED TEXT ADVENTURE\n\n
    How to:\n
    \tEnter arbitrary commands to proceed.\n
    Defined commands:
    \tType 'help' for help\n
    \tType 'pick up <thing>' or 'take <thing>' to add an object to your inventory.\n
    \tType 'i' or 'inv' or 'inventory' to see your inventory.\n
    \tType 'use <thing>' to use something in your inventory.\n
    \tType 'drop <thing>' to drop something in your inventory.\n
    \tType 'look around' to see what is around you.\n
    \tType 'quit()' or 'exit()' to exit.\n\n""")

    inventory = []
    dropped = []
    while True:
        val = input("$debd: ")
        if val == "i" or val == "inv" or val == "inventory":
            respond(" ".join(inventory))
        elif val.startswith("pick up "):
            inventory.append(val[8:])
        elif val.startswith("take "):
            inventory.append(val[5:])
        elif val.startswith("drop "):
            try:
                inventory.remove(val[5:])
                dropped.append(val[5:])
                respond("You dropped " + val[5:])
            except ValueError:
                print(val[5:] + " is not in your inventory.")
        elif val.startswith("use "):
            if val[4:] in inventory:
                respond("You use " + val[4:])
            else:
                print(val[4:] + " is not in your inventory.")
        elif val.startswith("look around"):
            print("You look around and see...")
            if dropped:
                respond(" ".join(dropped))
            else:
                print("nothing")
        elif val == "exit()" or val == "quit()":
            print("NO.")
        else:
            respond(random.choice(things))


if __name__ == "__main__":
    main()